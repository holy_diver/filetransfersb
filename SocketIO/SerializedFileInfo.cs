﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class SerializedFileInfo
{
    public enum FileType
    {
        Directory,
        File
    }
    public string Name;
    public byte[] Icon;
    public FileType Type;

    public void SetIcon(Icon icon)
    {
        MemoryStream ms = new MemoryStream();
        icon.Save(ms);
        this.Icon = ms.ToArray();
        ms.Close();
        ms.Dispose();
    }
}
