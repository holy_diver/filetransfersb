﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileTransferSB
{
    class Debugger
    {
        public static TextBox TextBox
        {
            get
            {
                return _textBox;
            }
            set
            {
                _textBox = value;
            }
        }

        private static TextBox _textBox;
        public static void WriteLine(string text, params object[] args)
        {
            text = String.Format(text, args);
            _textBox.Invoke(new Action(() =>
            {
                _textBox.AppendText(text + Environment.NewLine);
            }
            ));
        }
        public static void Write(string text, params object[] args)
        {
            text = String.Format(text, args);
            _textBox.Invoke(new Action(() =>
            {
                _textBox.AppendText(text);
            }
            ));
        }
    }
}
